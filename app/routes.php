<?php

/*
|--------------------------------------------------------------------------
| Fucking Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the fucking routes for an application.
| It's a fucking breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that fucking URI is requested.
|
*/

Route::group(array('prefix' => ''), function()
{
	Route::any('/', array('uses' => 'IndexController@index'));
});
