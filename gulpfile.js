'use strict';

var gulp = require('gulp'); 
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var minify = require('gulp-minify-css');
var browserSync = require('browser-sync');
var browserify = require('browserify');
var transform = require('vinyl-transform');

gulp.task('browser-sync', function() 
{
    browserSync({
        host: 'fuback.local',
        port: 1337,
        logSnippet: false,
        notify: false
    });
});

gulp.task('html', function()
{
	return gulp.src('./app/views/')
		.pipe(browserSync.reload({stream:true}));
});

gulp.task('styles', function() 
{
    return gulp.src('./public/less/**/*.less')
    	.pipe(concat('style.min.css'))
    	.pipe(less())
    	.pipe(autoprefixer('last 2 version', '> 1%', 'Android', 'Chrome', 'Firefox', 'Explorer', 'iOS', 'Opera', 'Safari'))
    	.pipe(minify())
    	.pipe(gulp.dest('./public/dist/'))
    	.pipe(browserSync.reload({stream:true}));
});

gulp.task('scripts', function() 
{
	var browserified = transform(function(file) 
	{
    	var b = browserify(file);
    	
    	return b.bundle();
	});

    return gulp.src('./public/js/*.js')
    	.pipe(jshint())
    	.pipe(jshint.reporter('default'))
    	.pipe(browserified)
    	.pipe(concat('index.min.js'))
    	.pipe(uglify())
    	.pipe(gulp.dest('./public/dist/'))
    	.pipe(browserSync.reload({stream:true}));
});

gulp.task('watch', ['browser-sync'], function() 
{
    gulp.watch('./app/views/**', ['html']);
    gulp.watch('./public/js/**', ['scripts']);
    gulp.watch('./public/less/**', ['styles']);
});

gulp.task('default', ['styles', 'scripts', 'watch']);